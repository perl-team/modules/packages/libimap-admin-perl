libimap-admin-perl (1.6.8-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libimap-admin-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 15 Oct 2022 18:17:19 +0100

libimap-admin-perl (1.6.8-1) unstable; urgency=medium

  [ gregor herrmann ]
  IGNORE-VERSION: 1.6.8-1
  # no code changes, only applies our patch

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 1.6.8
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Drop defined-array.patch, applied upstream

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!
  * Disable network tests during autopkgtests like during build.

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 00:41:26 +0100

libimap-admin-perl (1.6.7-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 16:40:45 +0100

libimap-admin-perl (1.6.7-2) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: update wording of Comment about copyright
    ownership.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add patch to fix "defined(@array)" error in test script.
    (Closes: #789338)

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Jun 2015 02:29:20 +0200

libimap-admin-perl (1.6.7-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Add partition support for the rename function
  * Short rules format (dh 7.0.50, quilt 0.46-7)
  * Rewrote control description
  * Remove duplicate Section/Priority information in binary paragraph
  * Add myself to Copyright and Uploaders
  * Updated copyright information

  [ Rene Mayorga ]
  * debian/control: update my email address.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Florian Schlichting ]
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Switched to source format 3.0 (quilt).
  * Bumped dh compatibility to level 8 (no changes necessary).
  * Added DEP-3 headers to no_interactive_tests.patch.
  * Fixed interpreter path on example scripts.
  * Added myself to Uploaders and copyright.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Mon, 09 Apr 2012 23:34:44 +0200

libimap-admin-perl (1.6.6-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/watch: use dist-based URL.
  * debian/rules: delete /usr/lib/perl5 only if it exists.

  [ Joachim Breitner ]
  * Removed myself from uploaders.

  [ Rene Mayorga ]
  * New upstream release
  * debian/control
    + Bump standards-version to 3.8.0
      + add README.Debian
    + set debhelper version to 7
    + add myself to uploaders
    + add ${misc:Depends} to Depends field
    + add Module name to long description
    + add quilt to b-d
  * debian/rules: refresh with the help of dh-make-perl
    + install examples/
  * debian/copyright - convert to the (new)format
  * no_interactive_tests.patch added
    + don't depend on internet conections nor user interactive test
      this patch just disabled (all?) tests.

  [ gregor herrmann ]
  * debian/watch: extended regexp for matching upstream releases.

 -- Rene Mayorga <rmayorga@debian.org.sv>  Sat, 13 Sep 2008 02:08:11 -0600

libimap-admin-perl (1.6.4-1) unstable; urgency=low

  [ Niko Tyni ]
  * New upstream release. (Closes: #329562)
  * Don't ignore the return code of 'make distclean'.
  * Upgrade to debhelper compatibility level 5.
  * Upgrade to Standards-Version 3.6.2. No changes needed.
  * Add myself to Uploaders.
  * Recommend libio-socket-ssl-perl and libdigest-hmac-perl.
  * Move debhelper from Build-Depends-Indep to Build-Depends,
    as per policy.

  [ Krzysztof Krzyzaniak (eloy) ]
  * Added debian/watch file

 -- Niko Tyni <ntyni@iki.fi>  Fri,  6 Jan 2006 19:06:01 +0200

libimap-admin-perl (1.6.1-1) unstable; urgency=low

  * Now Maintained by the Debian Perl Group (Closes: #274132)
  * New upstream release (Closes: #110063)
    + Handle whitespaces in Foldernames (Closes: #168618)
  * Acknowledge NMU (Closes: #190461)
  * Using dh_md5sums

 -- Joachim Breitner <nomeata@debian.org>  Fri,  8 Oct 2004 12:53:14 +0200

libimap-admin-perl (1.3.7-1.1) unstable; urgency=low

  * NMU.
  * Add Build-Depends-Indep on debhelper.  Closes: #190461.
  * Add INSTALLARCHLIB to Makefile.PL call, so that the Makefile won't
    try to install to the root filesystem.  Also remove this directory
    after install, to get rid of the .packlist file.

 -- Daniel Schepler <schepler@debian.org>  Sat,  2 Aug 2003 15:32:58 -0700

libimap-admin-perl (1.3.7-1) unstable; urgency=low

  * New upstream release

 -- Michael Alan Dorman <mdorman@debian.org>  Fri, 13 Oct 2000 08:28:40 -0400

libimap-admin-perl (1.3.0-1) unstable; urgency=low

  * New upstream release

 -- Michael Alan Dorman <mdorman@debian.org>  Sun, 27 Aug 2000 11:43:07 -0400

libimap-admin-perl (1.2.5-2) unstable; urgency=low

  * Fix a typo identified on the cyrus mailing list.

 -- Michael Alan Dorman <mdorman@debian.org>  Wed, 23 Aug 2000 09:25:27 -0400

libimap-admin-perl (1.2.5-1) unstable; urgency=low

  * New upstream release

 -- Michael Alan Dorman <mdorman@debian.org>  Mon, 10 Jul 2000 14:35:38 -0400

libimap-admin-perl (1.2.2-1) unstable; urgency=low

  * The author added a license for the package.

 -- Michael Alan Dorman <mdorman@debian.org>  Sat, 18 Mar 2000 17:14:23 -0500

libimap-admin-perl (1.2.1-1) unstable; urgency=low

  * Initial debianization

 -- Michael Alan Dorman <mdorman@debian.org>  Thu,  9 Mar 2000 11:43:26 -0500

Local variables:
mode: debian-changelog
user-mail-address: "mdorman@debian.org"
End:
